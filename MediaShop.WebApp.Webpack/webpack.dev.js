const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const webConfig = require('./conf/web.dev.json');

module.exports = merge(common.getConfig(webConfig), {
    plugins: [],
    devtool: 'source-map',
    devServer: {
        contentBase: './dist'
    },
    node : { fs: 'empty' }
});