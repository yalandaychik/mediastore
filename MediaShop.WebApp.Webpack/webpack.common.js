var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin')

exports.getConfig = (webConfig) => {
    let defineConfig = {};
    for (let key in webConfig) {
        if (webConfig.hasOwnProperty(key)) {
            defineConfig[key] = JSON.stringify(webConfig[key]);
        }
    }

    return {
        entry: {
            'polyfills': './src/polyfills.ts',
            'app': './src/main.ts'
        },
        output: {
            path: path.resolve(__dirname, 'dist'), // путь к каталогу выходных файлов - папка public
            publicPath: '/',
            filename: '[name].[hash].js'
        },
        devServer: {
            historyApiFallback: true,
        },
        resolve: {
            extensions: ['.ts', '.js']
        },
        module: {
            rules: [ //загрузчик для ts
                {
                    test: /\.ts$/, // определяем тип файлов
                    use: [{
                            loader: 'awesome-typescript-loader',
                            options: {
                                configFileName: path.resolve(__dirname, 'tsconfig.json')
                            }
                        },
                        'angular2-template-loader'
                    ]
                }, {
                    test: /\.html$/,
                    loader: 'html-loader'
                }, {
                    test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
                    loader: 'file-loader?name=assets/[name].[hash].[ext]'
                }, {
                    test: /\.css$/,
                    exclude: path.resolve(__dirname, './src/app'),
                    use: [
                        MiniCssExtractPlugin.loader,
                        "css-loader"
                    ]
                }, {
                    test: /\.css$/,
                    include: path.resolve(__dirname, './src/app'),
                    loader: 'raw-loader'
                }
            ]
        },
        plugins: [
            new CleanWebpackPlugin([path.resolve(__dirname, 'dist')]),
            new webpack.DefinePlugin(defineConfig),
            new webpack.ContextReplacementPlugin(
                /angular(\|\/)core/,
                path.resolve(__dirname, './src/'), // каталог с исходными файлами
                {} // карта маршрутов
            ),
            new HtmlWebpackPlugin({
                template: 'src/index.html'
            }),
            new MiniCssExtractPlugin({
                filename: "[name].css"
            }),
            new webpack.NoEmitOnErrorsPlugin(),
            new webpack.LoaderOptionsPlugin({
                htmlLoader: {
                    minimize: false
                }
            }),
            new CopyWebpackPlugin([
                {
                    from: 'web.config',
                    to: path.resolve(__dirname, 'dist', 'web.config'),
                    toType: 'file'
                }
            ])
        ]
    }
}