import { Component} from '@angular/core';

@Component({
    selector: 'my-app',
    template: `<h2>Hello Angular! Welcome Webpack!</h2> <pre>API Url: {{apiUrl}}</pre>`
})
export class AppComponent {
    apiUrl = API_URL
}