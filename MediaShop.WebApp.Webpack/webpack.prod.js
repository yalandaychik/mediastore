const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const webConfig = require('./conf/web.prod.json');
var webpack = require('webpack');

module.exports = merge(common.getConfig(webConfig), {
    plugins: [],
    node: {
        fs: 'empty'
    }
});