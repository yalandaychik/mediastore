﻿namespace MediaShop.Api.Configuration
{
    public class InformationOptions
    {
        public string Environment { get; set; }
        public int Counter { get; set; }
    }
}