﻿using MediaShop.Api.Configuration;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace MediaShop.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InfoController : ControllerBase
    {
        private readonly IOptions<InformationOptions> _options;
        public InfoController(IOptions<InformationOptions> options)
        {
            _options = options;
        }
        
        /// <summary>
        /// Generates system information for debugging!
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(200)]
        public string Get()
        {
            return $"{_options.Value.Environment} {_options.Value.Counter}";
        }
    }
}
